
# Run Locally
./mvnw spring-boot:run

# Docker Build
mvn clean install
docker build -t actuator-sample:1.0 .
docker run --name actuator-sample-app --rm -p 8080:8080 actuator-sample:1.0
Browse to http://localhost:8080
docker stop actuator-sample-app


